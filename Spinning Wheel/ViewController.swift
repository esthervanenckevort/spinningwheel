//
//  ViewController.swift
//  Spinning Wheel
//
//  Created by David van Enckevort on 11-06-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import UIKit
import SpriteKit
import os

class ViewController: UIViewController {
    @IBOutlet weak var wheel: SKView!
    @IBOutlet weak var reward: UILabel!
    @IBOutlet weak var spinWheelButton: UIButton!

    enum WheelState {
        case stopped(atAngle: Double)
        case spinning
        case starting
    }

    var wheelState = WheelState.stopped(atAngle: 0) {
        didSet {
            updateUI()
        }
    }

    private func updateUI() {
        switch wheelState {
        case .stopped(let angle):
            spinWheelButton?.isEnabled = true
            reward.isHidden = false
            reward.text = "You won \(gift(at: angle))"
        default:
            spinWheelButton?.isEnabled = false
            reward.isHidden = true
        }
    }

    private func gift(at angle: Double) -> String {
        let degrees = Measurement(value: angle, unit: UnitAngle.radians).converted(to: .degrees)
        print(degrees.value)
        switch degrees.value {
        case 0..<45: return "a bowtie"
        case 45..<90: return "a shirt"
        case 90..<135: return "pants"
        case 135..<180: return "glasses"
        case -45..<0: return "shoes"
        case -90..<(-45): return "a hat"
        case -135..<(-90): return "a jacket"
        case -180..<(-135): return "a rose"
        default:
            os_log("Unexpected value %d", [degrees.value])
        }
        return "nothing"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let scene = SKScene(fileNamed: "SpinningWheel") else { fatalError("Missing resources") }
        scene.backgroundColor = .white
        if let spinningWheel = scene.childNode(withName: "SpinningWheel") {
            let body = SKPhysicsBody(circleOfRadius: 20)
            body.angularDamping = 1
            body.mass = 20
            spinningWheel.physicsBody = body
        }
        scene.delegate = self
        wheel.presentScene(scene)
    }

    @IBAction func spinWheel(_ sender: UIButton) {
        switch wheelState {
        case .stopped(_):
            wheelState = .starting
        default:
            break
        }
    }

}

extension ViewController: SKSceneDelegate {
    func update(_ currentTime: TimeInterval, for scene: SKScene) {
        // Note Apple docs say that you should only update nodes in the update function
        guard let wheel = wheel.scene?.childNode(withName: "SpinningWheel") else {
            return
        }
        switch wheelState {
        case .spinning:
            guard let body = wheel.physicsBody else { break }
            let angularVelocity = Double(body.angularVelocity)
            if (-0.05..<0.05).contains(angularVelocity) {
                wheelState = .stopped(atAngle: Double(wheel.zRotation))
                wheel.physicsBody?.angularVelocity = 0
            }
        case .starting:
            wheelState = .spinning
            wheel.physicsBody?.applyAngularImpulse(CGFloat.random(in: 1...5))
        default:
             break
        }
    }
}

